Note: Commit history for this project is not available as the new project created in GIT after moving from public to private mode. Only complete source code check-in and test cases related are available.

Parkinglot system developed in JAVA platform. ANT is used for build and deployment. JUnit test cases are added. Minimum JAR files are included in lib folder to set classpath for jUnit test cases.

Stack Used:
JAVA (min 8 version)
ANT (build and deployment)
JUNIT (version 4)

Clone
Download or clone from GIT repository : git clone https://manjunath317@bitbucket.org/manjunath317/parkinglot.git


Pre-Requisites:
Make sure you have ANT and min Java 8 version to compile the source code.
After cloning, test ANT and Java path is set. Verfiy the commands "ant" for ANT and "java" for JAVA respectively.

Compile, Build and jUnit Test Cases:
To build and compile the project using ant, execture command "ant -f build.xml"
Build file contains targets for clean, init, junit, compile, build-app, deploy and makedir. 
Use "build-app" target to compile and run jUnit test cases.


Run Program:
After build and run jUnit test cases successfully, execute main program. parking_lot.sh (shell script) is provided to run the program. 
Follow below steps to execute commands.

1. To run list of commands from input file, execute command
$ ./parking_lot file_inputs.txt


2. To run commands one by one, execute command
$ ./parking_lot