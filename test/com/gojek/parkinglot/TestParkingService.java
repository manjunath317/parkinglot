package com.gojek.parkinglot;

import static org.junit.Assert.*;

import java.util.List;
import java.util.TreeMap;

import org.junit.Test;

import com.gojek.parkinglot.dto.CarDTO;
import com.gojek.parkinglot.utils.ParkingSlot;
import com.gojek.parkinglot.utils.ParkingUtil;


/**
 * This class used to create test cases for parking services.
 * @author Manjunath Jakkandi
 *
 */
public class TestParkingService {
	
	/**
	 * This method to validate park car in parkinglot.
	 * @author Manjunath Jakkandi
	 * @throws Exception
	 */
	@Test
	public void testParkCar() throws Exception{
		MockParkingLot.createParkingSlot();
		ParkingUtil.parkVehicle("KAA-01A-HHA-1234", "White");
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		System.out.println(parkedVehicleList);
		assertEquals(1, parkedVehicleList.size());
		//assertEquals(parkedVehicleList.get(0).getRegNumber(), "KAA-01A-HHA-1234"); // check exact parking vehicle with registration number exists in parkinglot
	}
	
	
	/**
	 * This method used to validate exit car from parkinglot.
	 * @author Manjunath Jakkandi
	 * @throws Exception
	 */
	@Test
	public void testExitCar() throws Exception {
		MockParkingLot.createParkingSlot();
		MockParkingLot.mockParkCars();
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		ParkingUtil.exitVehicle("1");
		assertTrue(!parkedVehicleList.containsKey(1));
		
	}
	
	/**
	 * This method used to validate get list of cars with color.
	 * @author Manjunath Jakkandi
	 */
	@Test
	public void testCarListWithColorCommand() throws Exception{
		MockParkingLot.createParkingSlot();
		MockParkingLot.mockParkCars();
		List<CarDTO> carList = ParkingUtil.getCarList("White");
		assertEquals(3, carList.size());
	}
	
	
	/**
	 * This method used to validate get list of car slots with specific color.
	 * @author Manjunath Jakkandi
	 */
	@Test
	public void testCarSlotsWithColorCommand() throws Exception{
		MockParkingLot.createParkingSlot();
		MockParkingLot.mockParkCars();
		List<CarDTO> carList = ParkingUtil.getSlotList("White");
		assertEquals(3, carList.size());
	}
	
	
	/**
	 * This method used to validate get slot number for a given car register number.
	 * @author Manjunath Jakkandi
	 */
	@Test
	public void testSlotWithRegNumber() throws Exception{
		MockParkingLot.createParkingSlot();
		MockParkingLot.mockParkCars();
		assertTrue(ParkingUtil.getSlot("KAA-01A-HHA-1238") != -1 && ParkingUtil.getSlot("KAA-01A-HHA-1238") < 6); // red colour
	}
	
	

}
