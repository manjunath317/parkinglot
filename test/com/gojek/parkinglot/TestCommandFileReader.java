package com.gojek.parkinglot;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

/**
 * This test class used to mock input command file exists in preferred location or not.
 * @author Manjunath Jakkandi
 *
 */
public class TestCommandFileReader {

	/**
	 * @author Manjunath Jakkandi
	 * This test case verify input file exists in preferred location or not. If not exists,
	 * test case fails. Else, execute successfully.
	 */
	@Test
	public void testCommandFileExists() throws Exception {
		Path file=Paths.get(MockParkingLot.filePath);
		assertTrue("is file_input.txt exists?", file.toFile().exists());
	}
	

}
