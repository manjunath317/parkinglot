package com.gojek.parkinglot;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * TO DO List - identify the list of test cases. 
 *  
 *  1. input file exists or not.
 *  2. verify the command. If command is wrong, fail the test case.
 *  3. test parking
 *  4. test exit parking
 *  5. test parking status >> Not required
 *  6. test car list with color
 *  7. test car slot with color
 *  8. test car slot with registration number
 *  9. test clear all slots >> not required
 *  
 *  Create test class for command file reader
 *  Create test class for commands processor
 *  Create test class for parking services.
 */

/**
 * This class used to create test suite to execute all the test cases.
 * @author Manjunath Jakkandi
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ TestCommandFileReader.class, TestCommandProcessor.class, TestParkingService.class })
public class AllTests {
	
}

