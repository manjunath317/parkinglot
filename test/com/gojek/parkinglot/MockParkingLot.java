package com.gojek.parkinglot;

import java.util.List;

import com.gojek.parkinglot.utils.CommandFileReader;
import com.gojek.parkinglot.utils.ParkingUtil;

/**
 * This class used as helper class to create all utility methods to execute jUnit test cases.
 * @author Manjunath Jakkandi
 *
 */
public class MockParkingLot {
	
	public static final String filePath = "file_input.txt";
	
	/**
	 * Method to mock create commands list from given input file.
	 * @return List<String>
	 */
	public static List<String> commandsList() {
		CommandFileReader commandReader = new CommandFileReader();
		List<String> commandsList = commandReader.readCommandFile(filePath);
		return commandsList;
	}
	
	/**
	 * Method to mock create 6 parking slots
	 */
	public static void createParkingSlot() {
		ParkingUtil.clearAllSlots();
		ParkingUtil.createParkingSlots("6");
	}
	
	/**
	 * Method to mock park vehicles
	 */
	public static void mockParkCars() {
		ParkingUtil.parkVehicle("KAA-01A-HHA-1234", "White");
		ParkingUtil.parkVehicle("KAA-01A-HHA-1235", "Black");
		ParkingUtil.parkVehicle("KAA-01A-HHA-1237", "White");
		ParkingUtil.parkVehicle("KAA-01A-HHA-1238", "Red");
		ParkingUtil.parkVehicle("KAA-01A-HHA-1236", "White");
		ParkingUtil.parkVehicle("KAA-01A-HHA-1231", "Red");
	}
	
	
}
