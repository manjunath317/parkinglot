package com.gojek.parkinglot;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.gojek.parkinglot.constants.CommandConstants;

/**
 * This class used to validate all the commands exists in input file.
 * @author Manjunath Jakkandi
 *
 */
public class TestCommandProcessor {

	
	/**
	 * This method used to validate all the valid commands exists in input file or not.
	 * If not a valid command, test case fails and exit.
	 * @author manjunathj
	 */
	@Test
	public void testCommands() throws Exception{
		List<String> commandsList = MockParkingLot.commandsList();
		if(commandsList!=null && commandsList.size() > 0) {
			for(String commandItem : commandsList) {
				String[] command = commandItem.split("\\s+");
				if(!command[0].isEmpty()) {
					switch (command[0]) {
						case CommandConstants.COMMAND_CREATE_PARKING_SLOT:{
							assertTrue(CommandConstants.COMMAND_CREATE_PARKING_SLOT, true);
							break;
						}
						case CommandConstants.COMMAND_PARK :{
							assertTrue(CommandConstants.COMMAND_PARK, true);
							break;
						}
						case CommandConstants.COMMAND_LEAVE :{
							assertTrue(CommandConstants.COMMAND_LEAVE, true);
							break;
						}
						case CommandConstants.COMMAND_PARKING_STATUS :{
							assertTrue(CommandConstants.COMMAND_PARKING_STATUS, true);
							break;
						}
						case CommandConstants.COMMAND_CAR_LIST_WITH_COLOUR :{
							assertTrue(CommandConstants.COMMAND_CAR_LIST_WITH_COLOUR, true);
							break;
						}
						case CommandConstants.COMMAND_CAR_SLOTS_WITH_COLOUR :{
							assertTrue(CommandConstants.COMMAND_CAR_SLOTS_WITH_COLOUR, true);
							break;
						}
						case CommandConstants.COMMAND_SLOT_WITH_REG_NUM :{
							assertTrue(CommandConstants.COMMAND_SLOT_WITH_REG_NUM, true);
							break;
						}
						case CommandConstants.COMMAND_CLEAR_ALL_SLOTS :{
							assertTrue(CommandConstants.COMMAND_CLEAR_ALL_SLOTS, true);
							break;
						}
						default: {
							assertTrue("Invalid Command", false);
							break;
						}
					}
				}else {
					assertTrue("Invalid Command", false);
				}
			}
		}else {
			assertTrue("No commands found", false);
		}
	}

}
