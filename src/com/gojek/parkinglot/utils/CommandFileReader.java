package com.gojek.parkinglot.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.gojek.parkinglot.constants.CommandConstants;
import com.gojek.parkinglot.constants.MessageConstants;

/**
 * This class used to process list of command file and invoke respective events
 * @author Manjunath Jakkandi
 *
 */
public class CommandFileReader {
	
	
	/**
	 * Method to read list of commands from input file.
	 * @param filePath
	 * @return List of commands.
	 */
	public List<String> readCommandFile(String filePath){
		List<String> commandsList = new ArrayList<String>();
		try{
			Path file=Paths.get(filePath);
			InputStream is=Files.newInputStream(file);
			BufferedReader reader=new BufferedReader(new InputStreamReader(is));
			String line="";
			while ((line = reader.readLine()) != null) {
				commandsList.add(line.trim());
			}
		}catch(NoSuchFileException nfex) {
			System.out.println("Sorry, file not found "+filePath +". Please provide valid file name along with path.");
		}catch(FileNotFoundException fex){
			System.out.println("Sorry, file not found "+filePath +". Please provide valid file name along with path.");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return commandsList;
	}
	
	
	/**
	 * Method to execute command for park vehicle, leave vehicle, status etc,.
	 * @param commandItem
	 */
	public void executeCommand(String commandItem) {
		String[] command = commandItem.split("\\s+");
		if(!command[0].isEmpty()) {
			switch (command[0]) {
				case CommandConstants.COMMAND_CREATE_PARKING_SLOT:{
					ParkingUtil.createParkingSlots(command[1]);
					break;
				}
				case CommandConstants.COMMAND_PARK :{
					if(validateCommandPark(command)) {
						ParkingUtil.parkVehicle(command[1], command[2]);
					}
					break;
				}
				case CommandConstants.COMMAND_LEAVE :{
					if(validateCommandLeave(command)) {
						ParkingUtil.exitVehicle(command[1]);
					}
					break;
				}
				case CommandConstants.COMMAND_PARKING_STATUS :{
					if(validateCommandParkingStatus(command)) {
						ParkingUtil.parkingStatus();
					}
					break;
				}
				case CommandConstants.COMMAND_CAR_LIST_WITH_COLOUR :{
					if(validateCommandCarListWithColour(command)) {
						ParkingUtil.getCarList(command[1]);
					}
					break;
				}
				case CommandConstants.COMMAND_CAR_SLOTS_WITH_COLOUR :{
					if(validateCommandCarSlotsWithColour(command)) {
						ParkingUtil.getSlotList(command[1]);
					}
					break;
				}
				case CommandConstants.COMMAND_SLOT_WITH_REG_NUM :{
					if(validateCommandSlotWithRegNum(command)) {
						ParkingUtil.getSlot(command[1]);
					}
					break;
				}
				case CommandConstants.COMMAND_CLEAR_ALL_SLOTS :{
					ParkingUtil.clearAllSlots();
					break;
				}
				default: {
					logInvalidCommand();
					break;
				}
					
			}
		}
	}
	
	
	/**
	 * Method to process list of commands read from file or manual input.
	 * @param commandList
	 */
	public void processCommands(List<String> commandList) {
		for(String commandItem : commandList) {
			executeCommand(commandItem);	
		}
	}
	
	private boolean validateCommandSlotWithRegNum(String[] command) {
		// TODO : Add necessary validations here
		return true;
	}


	private boolean validateCommandCarSlotsWithColour(String[] command) {
		// TODO : Add necessary validations here
		return true;
	}


	private boolean validateCommandCarListWithColour(String[] command) {
		// TODO : Add necessary validations here
		return true;
	}


	private boolean validateCommandParkingStatus(String[] command) {
		// TODO : Add necessary validations here
		return true;
	}


	public boolean validateCommandLeave(String [] command) {
		// TODO : Add necessary validations here
		return true;
	}
	
	public boolean validateCommandPark(String[] command) {
		// TODO : Add necessary validations here
		return true;
	}
	
	public void logInvalidCommand() {
		System.out.println(MessageConstants.INVALID_COMMAND);
	}
	
}
