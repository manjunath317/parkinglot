package com.gojek.parkinglot.utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import com.gojek.parkinglot.constants.MessageConstants;
import com.gojek.parkinglot.dto.CarDTO;
import com.gojek.parkinglot.dto.SlotDTO;
import com.gojek.parkinglot.impl.ParkManager;
import com.gojek.parkinglot.impl.ParkManagerImpl;

/**
 * This class used as helper class to write generic functions and processing business rules.
 * @author Manjunath Jakkandi
 *
 */
public class ParkingUtil {
	
	private static int minRandNumber = 1;
	private static int maxRandNumber = 100;
	
	/**
	 * Method to create parking slots based on number of slots required. 
	 * @param numberOfSlots
	 */
	public static void createParkingSlots(String numberOfSlots) {
		int slots = Integer.parseInt(numberOfSlots);
		ParkingSlot.getInstance().setMaxSlots(slots);
		List<SlotDTO> slotList = ParkingSlot.getInstance().getSlotDetails();
		slotList.clear();  //Clear all slots before creating if already exists.
		for(int i=0; i< slots; i++) {
			slotList.add(new SlotDTO(i, getRandomDistance()));
		}
		Collections.sort(slotList,new SlotDistanceComparator());
		System.out.println(MessageFormat.format(MessageConstants.CREATE_PARKING_SLOT_MESSAGE, numberOfSlots));
	}
	
	/**
	 * Method to display list of sorted slot matrix with distance from exit door.
	 * @param slotList
	 * @return
	 */
	private static List<SlotDTO> getSortedSlotList(List<SlotDTO> slotList){
		try {
			if(slotList !=null && slotList.size() > 0) {
				for(SlotDTO slotDTO : slotList) {
					System.out.println("slotId: "+slotDTO.getSlotNumber() +" >>> slotDistance: "+slotDTO.getSlotDistance());
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Method to park vehicle for nearest slot available from slt matrix.
	 * @param regNumber
	 * @param colour
	 */
	public static void parkVehicle(String regNumber, String colour) {
		try {
			ParkManager manager = new ParkManagerImpl();
			manager.parkVehicle(new CarDTO(regNumber, colour));
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Method to leave vehicle from parking
	 * @param slotNumber
	 */
	public static void exitVehicle(String slotNumber) {
		try {
			ParkManager manager = new ParkManagerImpl();
			manager.leavePark(Integer.parseInt(slotNumber));	
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Method to get parking status of all slots. 
	 */
	public static void parkingStatus() {		 
		try {
			ParkManager manager = new ParkManagerImpl();
			manager.getStatus();
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Method to get list of parked cars for given color. 
	 * @param colour
	 */
	public static List<CarDTO> getCarList(String colour) {
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		Iterator<Integer> iterator = parkedVehicleList.keySet().iterator();
		CarDTO carDTO = null;
		List<CarDTO> carList = new ArrayList<CarDTO>();
		while(iterator.hasNext()) {
			Integer slotNumber = iterator.next();
			carDTO = parkedVehicleList.get(slotNumber);
			if(carDTO.getColour().equalsIgnoreCase(colour)) {
				carList.add(carDTO);
				System.out.printf(carDTO.getRegNumber()+", ");
			}
		}
		return carList;
	}
	
	
	/**
	 * Method to get list of slots numbers 
	 * @param colour
	 */
	public static List<CarDTO> getSlotList(String colour) {
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		Iterator<Integer> iterator = parkedVehicleList.keySet().iterator();
		CarDTO carDTO = null;
		List<CarDTO> carList = new ArrayList<CarDTO>();
		System.out.println("\n");
		while(iterator.hasNext()) {
			Integer slotNumber = iterator.next();
			carDTO = parkedVehicleList.get(slotNumber);
			if(carDTO.getColour().equalsIgnoreCase(colour)) {
				carList.add(carDTO);
				System.out.printf(slotNumber+", ");
			}
		}
		return carList;
	}
	
	/**
	 * Method to get the slot number for a given car registration number
	 * @param regNumber
	 */
	public static Integer getSlot(String regNumber) {
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		Iterator<Integer> iterator = parkedVehicleList.keySet().iterator();
		CarDTO carDTO = null;
		boolean isFoundFlag = false;
		Integer slotNumber;
		Integer slot = -1;  // extra variable for test case to validate get slot functionality.
		while(iterator.hasNext()) {
			slotNumber = iterator.next();
			carDTO = parkedVehicleList.get(slotNumber);
			if(carDTO.getRegNumber().equalsIgnoreCase(regNumber)) {
				isFoundFlag = true;
				System.out.println(slotNumber);
				slot = slotNumber;
				break;
			}
		}
		if(!isFoundFlag) {
			System.out.println("Not found");
		}
		return slot;
	}
	
	/**
	 * Method to clear all slots from parking lot.
	 */
	public static void clearAllSlots() {
		ParkingSlot.getInstance().getVehicleSlots().clear();
		System.out.println("All slots cleared");
	}
	
	/**
	 * Method to get random number between given min and max number. Min and Max numbers can be configurable.
	 * These random numbers used to set the distance for each slot from entrance door.
	 * @return
	 */
	public static int getRandomDistance() {
	    Random rand = new Random();	    
	    int randomNum = rand.nextInt((maxRandNumber - minRandNumber) + 1) + minRandNumber;
	    return randomNum;
	}

}
