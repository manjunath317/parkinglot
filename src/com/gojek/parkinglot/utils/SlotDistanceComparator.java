package com.gojek.parkinglot.utils;

import java.util.Comparator;

import com.gojek.parkinglot.dto.SlotDTO;

/**
 * This class used to sort list of slot details with respect to slot distance
 * @author Manjunath Jakkandi
 *
 */
public class SlotDistanceComparator implements Comparator<SlotDTO>{

	@Override
	public int compare(SlotDTO o1, SlotDTO o2) {
		if(o1.getSlotDistance() > o2.getSlotDistance()){
            return 1;
        } else {
            return -1;
        }
	}

}
