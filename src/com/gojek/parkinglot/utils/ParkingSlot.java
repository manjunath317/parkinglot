package com.gojek.parkinglot.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import com.gojek.parkinglot.dto.CarDTO;
import com.gojek.parkinglot.dto.SlotDTO;


/**
 * This class used to create singleton object for ParkingSlot. Parking slot contains a tree map
 * of vehicle slots. Each slot has id (number) and card object.
 * @author Manjunath Jakkandi
 *
 */
public class ParkingSlot {
	
	private static ParkingSlot parkingSlot;
	
	private TreeMap<Integer, CarDTO> vehicleSlots;
	private List<SlotDTO> slotDetails;
	private int maxSlots;
	
	private ParkingSlot(){
		// Do nothing
	}
	
	
	/**
	 * Method to get ParkingSlot singleton object.
	 * @return
	 */
	public static ParkingSlot getInstance(){
		synchronized(ParkingSlot.class) {
			if(parkingSlot == null){
				parkingSlot = new ParkingSlot();
			}
		}
		return parkingSlot;
	}
	


	/**
	 * Method to get vehicle slots object with single ton tree map list.
	 * @return
	 */
	public TreeMap<Integer, CarDTO> getVehicleSlots() {
		synchronized(ParkingSlot.class) {
			if(vehicleSlots == null) {
				vehicleSlots = new TreeMap<Integer, CarDTO>();
			}
		}
		return vehicleSlots;
	}

	/**
	 * Method to get slotDetails list object
	 * @return
	 */
	public List<SlotDTO> getSlotDetails(){
		synchronized(ParkingSlot.class) {
			if(slotDetails == null) {
				slotDetails = new ArrayList<SlotDTO>();
			}
		}
		return slotDetails;
	}
	
	
	public int getMaxSlots() {
		return maxSlots;
	}

	public void setMaxSlots(int maxSlots) {
		this.maxSlots = maxSlots;
	}

	
}
