package com.gojek.parkinglot.impl;

import com.gojek.parkinglot.dto.CarDTO;

/**
 * This class used to define the events for vehicle parking
 * @author Manjunath Jakkandi
 *
 */
public interface ParkManager {
	
	public void parkVehicle(CarDTO vehicle);
	
	public void leavePark(int SlotNumber);
	
	public void getStatus();
	
}
