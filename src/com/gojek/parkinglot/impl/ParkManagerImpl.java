package com.gojek.parkinglot.impl;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import com.gojek.parkinglot.constants.MessageConstants;
import com.gojek.parkinglot.dto.CarDTO;
import com.gojek.parkinglot.dto.SlotDTO;
import com.gojek.parkinglot.utils.ParkingSlot;

/**
 * This class used to create implementation for parking lot events.
 * @author Manjunath Jakkandi
 *
 */
public class ParkManagerImpl implements ParkManager{

	/**
	 * Method to park vehicle for nearest slot available from slt matrix.
	 */
	@Override
	public void parkVehicle(CarDTO car) {
		List<SlotDTO> slotDetailsList = ParkingSlot.getInstance().getSlotDetails();
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		if(parkedVehicleList.size() < ParkingSlot.getInstance().getMaxSlots()) {
			for(SlotDTO slot : slotDetailsList) {
				if(!parkedVehicleList.containsKey(slot.getSlotNumber())) {
					parkedVehicleList.put(slot.getSlotNumber(), car);
					System.out.println(MessageFormat.format(MessageConstants.PARK_CAR_MESSAGE, slot.getSlotNumber()));
					break;
				}
			}			
		}else {
			System.out.println(MessageFormat.format(MessageConstants.PARK_CAR_ERROR_MESSAGE, car.getRegNumber(), car.getColour()));
		}
	}

	/**
	 * Method to leave parking for given slot number.
	 */
	@Override
	public void leavePark(int slotNumber) {
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		if(parkedVehicleList.containsKey(slotNumber)) {
			parkedVehicleList.remove(slotNumber);
			System.out.println(MessageFormat.format(MessageConstants.LEAVE_PARKING_MESSAGE, slotNumber));
		}else {
			System.out.println(MessageFormat.format(MessageConstants.INVALID_SLOT_NUM_MESSAGE, slotNumber));
		}
	}

	/**
	 * Method to get the status of parking lot. Status should display Slot number, registration number and color.
	 */
	@Override
	public void getStatus() {
		TreeMap<Integer, CarDTO> parkedVehicleList = ParkingSlot.getInstance().getVehicleSlots();
		Iterator<Integer> iterator = parkedVehicleList.keySet().iterator();
		System.out.println("----------------------------------------------------------");
		System.out.printf( "%-15s %15s  %15s %n", "Slot No" , "Registration No", "Colour");
		System.out.println("----------------------------------------------------------");
		CarDTO carDTO = null;
		while(iterator.hasNext()) {
			Integer slotNumber = iterator.next();
			carDTO = parkedVehicleList.get(slotNumber);
			System.out.printf( "%-15s %15s %15s %n", slotNumber , carDTO.getRegNumber(), carDTO.getColour());
		}
		System.out.println("----------------------------------------------------------");
	}

}
