package com.gojek.parkinglot.constants;

/**
 * 
 * @author Manjunath Jakkandi
 * This class used to define all the commands used for parking lot system.
 */
public class CommandConstants {
	
	public static final String COMMAND_CREATE_PARKING_SLOT = "create_parking_lot";
	
	public static final String COMMAND_PARK = "park";
	
	public static final String COMMAND_LEAVE = "leave";
	
	public static final String COMMAND_PARKING_STATUS = "status";
	
	public static final String COMMAND_CAR_LIST_WITH_COLOUR = "registration_numbers_for_cars_with_colour";
	
	public static final String COMMAND_CAR_SLOTS_WITH_COLOUR = "slot_numbers_for_cars_with_colour";
	
	public static final String COMMAND_SLOT_WITH_REG_NUM = "slot_number_for_registration_number";
	
	public static final String COMMAND_CLEAR_ALL_SLOTS = "clear_all_slots";
	
	
}
