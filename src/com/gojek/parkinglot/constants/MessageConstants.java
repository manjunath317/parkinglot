package com.gojek.parkinglot.constants;

/**
 * This class used to display messages with dynamic parameters using messageFormat.
 * @author Manjunath Jakkandi
 *
 */
public class MessageConstants {
	
	public static final String INVALID_COMMAND="Sorry, invalid command received.";
	
	public static final String CREATE_PARKING_SLOT_MESSAGE="Created a parking lot with {0} slots";
	
	public static final String WELCOME_MESSAGE1="Welcome to parkinglot system. Please provide the command";
	
	public static final String WELCOME_MESSAGE2="To exit application, give command - exit";
	
	public static final String INVALID_SLOT_NUM_MESSAGE="Sorry...leave {0} is not a valid command";
	
	public static final String LEAVE_PARKING_MESSAGE="Slot number {0} is free";
	
	public static final String PARK_CAR_MESSAGE="Allocated slot number {0}";
	
	public static final String PARK_CAR_ERROR_MESSAGE="\nSorry, parking lot is full";
}
