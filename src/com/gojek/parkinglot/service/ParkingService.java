package com.gojek.parkinglot.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.gojek.parkinglot.constants.MessageConstants;
import com.gojek.parkinglot.utils.CommandFileReader;

/**
 * This class used to initiate the service for parking lot.
 * @author Manjunath Jakkandi
 *
 */
public class ParkingService {
	public static void main(String[] args) {
		CommandFileReader commandReader = new CommandFileReader();
		try {
			if(args.length > 0) {
				String filePath = args[0];
				List<String> commandsList = commandReader.readCommandFile(filePath);
				commandReader.processCommands(commandsList);
			}else {
				System.out.println(MessageConstants.WELCOME_MESSAGE1);
				System.out.println(MessageConstants.WELCOME_MESSAGE2);
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				boolean exitFlag = true;
				while(exitFlag) {
					String s = bufferedReader.readLine().trim();
					if(s!=null && !s.isEmpty() && s.equalsIgnoreCase("exit")) {
						break;
					}else {
						commandReader.executeCommand(s);
					}
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}	
	}
	
	
}
