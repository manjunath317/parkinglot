package com.gojek.parkinglot.dto;

import java.io.Serializable;


/**
 * This class used to define the slot details such as slot number and distance from exit door.
 * @author Manjunath Jakkandi
 *
 */
public class SlotDTO implements Serializable{

	private int slotNumber;
	private int slotDistance;
	
	public SlotDTO() {
		super();
	}
	
	public SlotDTO(int slotNumber, int slotDistance) {
		this.slotNumber = slotNumber;
		this.slotDistance = slotDistance;
	}
	
	public int getSlotNumber() {
		return slotNumber;
	}
	public void setSlotNumber(int slotNumber) {
		this.slotNumber = slotNumber;
	}
	public int getSlotDistance() {
		return slotDistance;
	}
	public void setSlotDistance(int slotDistance) {
		this.slotDistance = slotDistance;
	}	
	
}
