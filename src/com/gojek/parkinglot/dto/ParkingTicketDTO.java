package com.gojek.parkinglot.dto;

import java.io.Serializable;


/**
 * This class used to generate ticket number for car with particular slot.
 * @author Manjunath Jakkandi
 *
 */
public class ParkingTicketDTO implements Serializable{

	private CarDTO carDTO;
	private int slotNumber;
	private long ticketNumber;
	
	public ParkingTicketDTO() {
		super();
	}
	
	public ParkingTicketDTO(CarDTO carDTO, int slotNumber) {
		this.carDTO = carDTO;
		this.slotNumber = slotNumber;
	}
	
	public ParkingTicketDTO(CarDTO carDTO, int slotNumber, long ticketNumber) {
		this.carDTO = carDTO;
		this.slotNumber = slotNumber;
		this.ticketNumber = ticketNumber;
	}
	
	public CarDTO getCarDTO() {
		return carDTO;
	}
	public void setCarDTO(CarDTO carDTO) {
		this.carDTO = carDTO;
	}
	public int getSlotNumber() {
		return slotNumber;
	}
	public void setSlotNumber(int slotNumber) {
		this.slotNumber = slotNumber;
	}
	public long getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(long ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	
}
