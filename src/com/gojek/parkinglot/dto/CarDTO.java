package com.gojek.parkinglot.dto;

import java.io.Serializable;

/**
 * This class used to set properties for Vehicle.
 * @author Manjunath Jakkandi
 *
 */

public class CarDTO implements Serializable{

	private String colour;
	private String regNumber;
	
	public CarDTO() {
		super();
	}
	
	
	public CarDTO(String regNumber, String colour) {
		this.colour = colour;
		this.regNumber = regNumber;
	}
	
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	
}
