echo "@author - Manjunath Jakkandi"
echo "This script used to run parkinglot.jar pre-created using ant build file."
echo "To launch manual command program execute ./parking_lot.sh"
echo "To launch auto mode, provide file name. ./parking_lot.sh <file_name>"
echo "Note - above file must be absolute path"

 
echo "You have provided file path -  $1"

sudo chmod 755 ./dist/parkinglot.jar
sudo chmod 755 $1

java -jar ./dist/parkinglot.jar $1
